{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -fno-warn-missing-signatures #-}

module FastCut.ProjectTest where

import           Data.Semigroup   ((<>))
import           Test.Tasty.Hspec

import           FastCut.Sequence

{-# ANN module ("HLint: ignore Use camelCase" :: String) #-}
